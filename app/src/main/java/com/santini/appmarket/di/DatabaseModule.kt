package com.santini.appmarket.di

import android.content.Context
import androidx.room.Room
import com.santini.appmarket.data.AppDatabase
import com.santini.appmarket.data.CarritoDao
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.android.qualifiers.ApplicationContext
import dagger.hilt.components.SingletonComponent
import javax.inject.Singleton


@Module
@InstallIn(SingletonComponent::class)
class DatabaseModule {
    @Singleton
    @Provides

    fun provideRoomInstance(@ApplicationContext context: Context): AppDatabase =
        //CREAR BASE DE DATOS
        Room.databaseBuilder(
            context,
            AppDatabase::class.java,
            "bdMarket"
        ).build()

    // injecto el DAO
    @Singleton
    @Provides
    fun provideCarritoDao(db: AppDatabase): CarritoDao = db.carritoDao()
}