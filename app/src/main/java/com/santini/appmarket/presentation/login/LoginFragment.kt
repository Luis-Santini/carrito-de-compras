package com.santini.appmarket.presentation.login

import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.lifecycle.Observer
import androidx.navigation.Navigation
import com.santini.appmarket.MenuActivity
import com.santini.appmarket.R
import com.santini.appmarket.core.SecurityPrefences
import com.santini.appmarket.core.SecurityPrefences.encrypterPreferences
import com.santini.appmarket.databinding.FragmentLoginBinding
import com.santini.appmarket.util.Constantes
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class LoginFragment : Fragment() {

    private lateinit var binding: FragmentLoginBinding // para el viewBinding
    private val viewModel: LoginViewModel by viewModels()
    private lateinit var globalView: View


    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_login, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        binding = FragmentLoginBinding.bind(view) // para el viewBinding
        globalView = view
        init()
        setUpObservers()
    }

    private fun init() {
        binding.btnIngresar.setOnClickListener {
            val email = binding.textInputLayoutCorreo.editText?.text.toString()
            val password = binding.textInputLayoutClave.editText?.text.toString()
            viewModel.auntenticar(email, password)
        }

        binding.btnCrearCuenta.setOnClickListener {
            Navigation.findNavController(globalView).navigate(R.id.action_loginFragment_to_registroFragment)

        }
    }

    private fun setUpObservers() {
        viewModel.lodaer.observe(viewLifecycleOwner, Observer {
            if (it == true) {
                binding.progressBar.visibility = View.VISIBLE
            } else {
                binding.progressBar.visibility = View.GONE
            }
        })

        viewModel.error.observe(viewLifecycleOwner, Observer {
            Toast.makeText(requireContext(), it.toString(), Toast.LENGTH_LONG).show()
        })

        viewModel.usuario.observe(viewLifecycleOwner, Observer {
            it.let {
               // Toast.makeText(requireContext(), it.uuid.toString(), Toast.LENGTH_LONG).show()
                //NAVEGAR JASTA HACIA LA ACTIVIDAD DEL MENU
                val intent = Intent(requireContext(), MenuActivity::class.java)
                startActivity(intent)
            }

        })

        viewModel.token.observe(viewLifecycleOwner, Observer {
            //Guardarlo en una perferencia
            val preferences = requireContext().encrypterPreferences(Constantes.PREFERENCIA_LOGIN)
            SecurityPrefences.saveToken(it, preferences)


        })
    }


}