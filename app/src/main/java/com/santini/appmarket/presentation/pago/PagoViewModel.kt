package com.santini.appmarket.presentation.pago

import androidx.hilt.lifecycle.ViewModelInject
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.santini.appmarket.data.Api
import com.santini.appmarket.data.CarritoDao
import com.santini.appmarket.model.CarritoDbDto
import com.santini.appmarket.model.PagoRequest
import com.santini.appmarket.util.Constantes
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import retrofit2.Retrofit
import java.lang.Exception
import javax.inject.Inject

@HiltViewModel
class PagoViewModel
@Inject
constructor(
    private val carritoDao: CarritoDao,
    private val provideRetrofit: Api.ApiInterface
) : ViewModel() {
    private val _error = MutableLiveData<String>()
    val error: LiveData<String> = _error

    private val _loader = MutableLiveData<Boolean>()
    val loader: LiveData<Boolean> = _loader

    private val _ordenCompra = MutableLiveData<String>()
    val ordenCompra : LiveData<String> = _ordenCompra

    val productosCarritos = carritoDao.obtenerProductosCarrito()

    fun pagarCarrito(token: String, pagoRequest: PagoRequest) {
        viewModelScope.launch {
            _loader.value = true
            try {
                val result = withContext(Dispatchers.IO){
                    provideRetrofit.pagar("${Constantes.BEARER} $token",pagoRequest)
                }

                if (result.isSuccessful) {
                    //Borrar la tabla carrito
                    withContext(Dispatchers.IO){
                        val carrito = pagoRequest.productos.map {
                            CarritoDbDto(uuid = it.productoId)
                        }

                        carritoDao.eliminarProductosCarrito(carrito)
                    }
                    _ordenCompra.value = result?.body()?.data

                }else{
                    _error.value = result.message().toString()
                }
           } catch (e: Exception) {
                _error.value = e.message
            } finally {
               _loader.value = false
            }
        }
    }

}