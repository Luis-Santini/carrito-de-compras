package com.santini.appmarket.presentation.carrito

import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.santini.appmarket.R
import com.santini.appmarket.databinding.ItemCarritoBinding
import com.santini.appmarket.model.CarritoDbDto

class CarritoAdapter(
    var carrito: List<CarritoDbDto> = listOf(),
    var callBackItemEliminar: (CarritoDbDto) -> Unit
) :
    RecyclerView.Adapter<CarritoAdapter.CarritoAdapterViewHolder>() {

//crear otro callback para actualizar

    lateinit var callBackItemEditar: (CarritoDbDto) -> Unit


// 2 crear una clase viewHolder
    // mecesita la DATA Y XML




    inner class CarritoAdapterViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        private val binding: ItemCarritoBinding = ItemCarritoBinding.bind(itemView)

        fun bind(producto: CarritoDbDto) = with(binding) {
            tvDescripcionCarrito.text = producto.descripcion
            tvCantidadCarrito.text = "${producto.cantidad}"
            val costo = producto.cantidad * producto.precio
            val costoFormateado = "%.2f".format(costo)
            tvPrecioCarrito.text = costoFormateado
            imgProductoCarrito.setImageBitmap(convertByesToImage(producto.imagen))

            imgEliminarCarrito.setOnClickListener {
                callBackItemEliminar(producto)
            }

            imgEditarCarrito.setOnClickListener {
                callBackItemEditar(producto)
            }

        }

        fun convertByesToImage(byteArray: ByteArray): Bitmap {
            return BitmapFactory.decodeByteArray(
                byteArray, 0, byteArray.size
            )
        }

    }

    fun updateLista(carrito: List<CarritoDbDto>) {
        this.carrito = carrito
        notifyDataSetChanged()
    }

    //INYECTAR LA VISTA
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): CarritoAdapterViewHolder {

        val view =
            LayoutInflater.from(parent.context).inflate(R.layout.item_carrito, parent, false)
        return CarritoAdapterViewHolder(view)
    }

    // recuperamos la data
    override fun onBindViewHolder(holder: CarritoAdapterViewHolder, position: Int) {
        val producto = carrito[position]
        holder.bind(producto)

    }

    override fun getItemCount(): Int {
        return carrito.size
    }


}