package com.santini.appmarket.presentation.categoria

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.santini.appmarket.data.Api
import com.santini.appmarket.model.Categoria
import com.santini.appmarket.util.Constantes
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import java.lang.Exception

class CategoriaViewModel : ViewModel() {
    private val _loader = MutableLiveData<Boolean>()
    val loader: LiveData<Boolean> = _loader

    private val _error = MutableLiveData<String>()
    val error: LiveData<String> = _error

    private val _categorias = MutableLiveData<List<Categoria>>()
    val categorias: LiveData<List<Categoria>> = _categorias

    fun obtenerCategorias(token: String) {
        viewModelScope.launch {
            _loader.value = true
            try {

                val resultado = withContext(Dispatchers.IO) {
                    Api.build().obtenerCategorias("${Constantes.BEARER} $token")
                }
                if (resultado.isSuccessful) {
                    _categorias.value = resultado.body()?.data

                } else {
                    _error.value = resultado.message()
                }

            } catch (ex: Exception) {
                _error.value = ex.message.toString()
            } finally {
                _loader.value = false
            }

        }

    }

}