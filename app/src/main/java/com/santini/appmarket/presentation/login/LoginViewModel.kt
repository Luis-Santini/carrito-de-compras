package com.santini.appmarket.presentation.login

import androidx.hilt.lifecycle.ViewModelInject
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.santini.appmarket.data.Api
import com.santini.appmarket.model.LoginRequest
import com.santini.appmarket.model.Usuario
import dagger.hilt.android.AndroidEntryPoint
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import javax.inject.Inject

@HiltViewModel
class LoginViewModel
@Inject //@ViewModelInject
constructor(private val provideRetrofit: Api.ApiInterface
           /* private val usuarioMapper: UsuarioMapper,
            private val usuarioRepository: UsuarioRepository*/
    )

    : ViewModel() {
    private val _loader = MutableLiveData<Boolean>()
    val lodaer: LiveData<Boolean> = _loader
    private val _error = MutableLiveData<String>()
    val error: LiveData<String> = _error
    private val _usuario = MutableLiveData<Usuario>()
    val usuario: LiveData<Usuario> = _usuario

    private val _token = MutableLiveData<String>()
    val token: LiveData<String> = _token


    fun auntenticar(email: String, passsword: String) {
        viewModelScope.launch {
            _loader.value = true

            try {
                val resultado = withContext(Dispatchers.IO) {
                   // Api.build().autenticar(LoginRequest(email, passsword))
                    provideRetrofit.autenticar(LoginRequest(email, passsword))
                }

                  if (resultado.isSuccessful) {
                       if (resultado.body()?.success == true) {
                           //credenciales correctas
                           _loader.value = false
                            _token.value =  resultado.body()?.token

                           _usuario.value = resultado.body()?.data
                       } else {
                           _loader.value = false
                           _error.value = resultado.body()?.message
                       }
                   } else {
                       _loader.value = false
                       _error.value = resultado.body()?.message
                   }
            } catch (ex: Exception) {
                _error.value = ex.message.toString()
            } finally {
                _loader.value = false
            }
        }
    }
}