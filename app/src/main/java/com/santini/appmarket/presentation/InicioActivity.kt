package com.santini.appmarket.presentation

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.santini.appmarket.R
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class  InicioActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_inicio)
    }
}