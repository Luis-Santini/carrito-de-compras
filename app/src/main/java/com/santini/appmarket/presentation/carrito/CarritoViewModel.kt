package com.santini.appmarket.presentation.carrito

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.santini.appmarket.data.CarritoDao
import com.santini.appmarket.model.CarritoDbDto
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import java.lang.Exception
import javax.inject.Inject

@HiltViewModel
class CarritoViewModel
@Inject
constructor(private val carritoDao: CarritoDao) : ViewModel() {


    private val _loader = MutableLiveData<Boolean>()
    val loader: LiveData<Boolean> = _loader

    private val _error = MutableLiveData<String>()
    val error: LiveData<String> = _error

    private val _carrito = MutableLiveData<List<CarritoDbDto>>()
    val carrito: LiveData<List<CarritoDbDto>> = _carrito

    private val _mensajeProductoEliminado = MutableLiveData<String>()
    val mensajeProductoEliminado: LiveData<String> = _mensajeProductoEliminado

    private val _mensajeProductoActualizado = MutableLiveData<String>()
    val mensajeProductoActualizado: LiveData<String> = _mensajeProductoActualizado

    val productoCarrito = carritoDao.obtenerProductosCarrito()


  /*  fun obtenerProductoCarrito() {
       _loader.value = true
        viewModelScope.launch {
            try {
                val resultado = withContext(Dispatchers.IO) {
                    carritoDao.obtenerProductosCarritos()

                }
                _carrito.value = resultado
            } catch (ex: Exception) {
                _error.value = ex.message.toString()
            } finally {
               _loader.value = false
            }


        }
    }*/

    fun eliminarProductoCarrito(dto: CarritoDbDto) {
        _loader.value = true

        viewModelScope.launch {
            try {
                 withContext(Dispatchers.IO) {
                    carritoDao.eliminar(dto)

                }
                _mensajeProductoEliminado.value = "Producto eliminado ${dto.uuid }del carrito"
            } catch (ex: Exception) {
                _error.value = ex.message.toString()
            } finally {
                _loader.value = false
            }
        }
    }

    fun actualizar(carritoDbDto: CarritoDbDto) {
        viewModelScope.launch {
            _loader.value = true
            try {
                withContext(Dispatchers.IO) {
                    carritoDao.actualizar(carritoDbDto)
                }
                _mensajeProductoActualizado.value = "Producto Actualizado ${carritoDbDto.descripcion }del carrito"

            } catch (ex: Exception) {
                _error.value = ex.message.toString()
            } finally {
                _loader.value = false
            }
        }

    }
}