package com.santini.appmarket.presentation.producto.cabecera

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.santini.appmarket.R
import com.santini.appmarket.databinding.ItemProductosBinding
import com.santini.appmarket.model.Producto
import com.squareup.picasso.Picasso

class ProductoAdapater(
    var productos: List<Producto> = listOf(),
    var callBackItemProducto: (Producto) -> Unit
) :
    RecyclerView.Adapter<ProductoAdapater.ProductoAdapterViewHolder>() {
// 2 crear una clase viewHolder
    // mecesita la DATA Y XML

    inner class ProductoAdapterViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        private val binding: ItemProductosBinding = ItemProductosBinding.bind(itemView)
        //ItemCategoriaBinding = ItemCategoriaBinding.bind(itemView)

        fun bind(producto: Producto) =
            with(binding) {
                tvCodigo.text = producto.codigo
                tvDescripcion.text = producto.descripcion
                tvPrecio.text = "$ ${producto.precio}"

                Picasso.get().load(producto.imagenes[0]).error(R.drawable.logo_bienvenida).into(binding.imgProducto)
                root.setOnClickListener {
                    callBackItemProducto(producto)
                }
            }
    }

    fun updateLista(productos: List<Producto>) {
        this.productos = productos
        notifyDataSetChanged()
    }

    //INYECTAR LA VISTA
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ProductoAdapterViewHolder {

        val view =
            LayoutInflater.from(parent.context).inflate(R.layout.item_productos, parent, false)
        return ProductoAdapterViewHolder(view)
    }

    // recuperamos la data
    override fun onBindViewHolder(holder: ProductoAdapterViewHolder, position: Int) {
        val producto = productos[position]
        holder.bind(producto)

    }

    override fun getItemCount(): Int {
        return productos.size
    }

}