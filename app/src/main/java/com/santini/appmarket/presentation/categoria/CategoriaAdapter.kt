package com.santini.appmarket.presentation.categoria

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.santini.appmarket.R
import com.santini.appmarket.databinding.ItemCategoriaBinding
import com.santini.appmarket.model.Categoria
import com.squareup.picasso.Picasso

//1 Declarar una lista vacia
// 3 implementar los metodos del adapter
class CategoriaAdapter(var categorias: List<Categoria> = listOf(), var callBackItemCategoria : (Categoria) -> Unit) :
    RecyclerView.Adapter<CategoriaAdapter.CategoriaAdapterViewHolder>() {
// 2 crear una clase viewHolder
    // mecesita la DATA Y XML

    inner class CategoriaAdapterViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        private val binding: ItemCategoriaBinding = ItemCategoriaBinding.bind(itemView)

        fun bind(categorias: Categoria) {

            Picasso.get().load(categorias.cover).into(binding.imgNombreCategiria)

            binding.root.setOnClickListener {
                callBackItemCategoria(categorias)
            }
        }
    }

    fun updateLista(categorias: List<Categoria>) {
        this.categorias = categorias
        notifyDataSetChanged()
    }

    //INYECTAR LA VISTA
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): CategoriaAdapterViewHolder {

        val view =
            LayoutInflater.from(parent.context).inflate(R.layout.item_categoria, parent, false)
        return CategoriaAdapterViewHolder(view)
    }

    // recuperamos la data
    override fun onBindViewHolder(holder: CategoriaAdapterViewHolder, position: Int) {
        val categoria = categorias[position]
        holder.bind(categoria)

    }

    override fun getItemCount(): Int {
        return categorias.size
    }


}