package com.santini.appmarket.presentation.producto.detalle

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.santini.appmarket.data.CarritoDao
import com.santini.appmarket.model.CarritoDbDto
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import javax.inject.Inject

@HiltViewModel
class DetalleProductoViewModel
@Inject
constructor(private val carritoDao: CarritoDao) : ViewModel() {

    private val _loader = MutableLiveData<Boolean>()
    val loader: LiveData<Boolean> = _loader

    private val _error = MutableLiveData<String>()
    val error: LiveData<String> = _error

    private val _respuesta = MutableLiveData<String>()
    val respuesta: LiveData<String> = _respuesta
    fun grabarProdutoCarrito(dto: CarritoDbDto) {

        viewModelScope.launch {
            _loader.value = true
            try {
                //BASE DE DATOS LOCAL
                val resultado = withContext(Dispatchers.IO) {
                    carritoDao.insertar(dto)
                }

                if (resultado?.toInt() > 0) {
                    _respuesta.value = "Producto Agregadp añ carro"
                } else {
                    _error.value = "Hubo problemas al agregar el producto en el carrito"
                }
            } catch (ex: Exception) {
                _error.value = ex.message.toString()
            } finally {
                _loader.value = false
            }
        }
    }
}