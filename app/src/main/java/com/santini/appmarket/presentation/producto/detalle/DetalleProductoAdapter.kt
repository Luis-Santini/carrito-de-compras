package com.santini.appmarket.presentation.producto.detalle

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.santini.appmarket.R
import com.santini.appmarket.databinding.ItemCategoriaBinding
import com.santini.appmarket.databinding.ItemProductosSecundariosBinding
import com.santini.appmarket.model.Categoria
import com.santini.appmarket.presentation.categoria.CategoriaAdapter
import com.squareup.picasso.Picasso

class DetalleProductoAdapter (var productosSecundarios: List<String> = listOf()) :
    RecyclerView.Adapter<DetalleProductoAdapter.DetalleProductoAdapterViewHolder>() {
// 2 crear una clase viewHolder
    // mecesita la DATA Y XML

    inner class DetalleProductoAdapterViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        private val binding: ItemProductosSecundariosBinding = ItemProductosSecundariosBinding.bind(itemView)

        fun bind(imagen: String) {

            Picasso.get().load("$imagen").error(R.drawable.logo_bienvenida).into(binding.imgProductoSecundario)
        }
    }

    fun updateLista(productosSecundarios: List<String>) {
        this.productosSecundarios = productosSecundarios
        notifyDataSetChanged()
    }

    //INYECTAR LA VISTA
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): DetalleProductoAdapterViewHolder {

        val view =
            LayoutInflater.from(parent.context).inflate(R.layout.item_productos_secundarios, parent, false)
        return DetalleProductoAdapterViewHolder(view)
    }

    // recuperamos la data
    override fun onBindViewHolder(holder: DetalleProductoAdapterViewHolder, position: Int) {
        val imagen = productosSecundarios[position]
        holder.bind(imagen)

    }

    override fun getItemCount(): Int {
        return productosSecundarios.size
    }
}