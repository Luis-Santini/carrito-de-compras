package com.santini.appmarket.presentation.registro

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.AdapterView
import android.widget.ArrayAdapter
import android.widget.Toast
import androidx.fragment.app.viewModels
import androidx.lifecycle.Observer
import com.kaopiz.kprogresshud.KProgressHUD
import com.santini.appmarket.R
import com.santini.appmarket.databinding.FragmentRegistroBinding
import com.santini.appmarket.model.Genero
import com.santini.appmarket.model.RegistroRequest


class RegistroFragment : Fragment() {
    private lateinit var binding: FragmentRegistroBinding

    private val viewModel: RegistroViewModel by viewModels()

    private lateinit var progress: KProgressHUD

    private var genero = ""
    private var generos = listOf<Genero>()

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_registro, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        binding = FragmentRegistroBinding.bind(view)
        init()
        setupObservers()
    }

    private fun setupObservers() {
        viewModel.loader.observe(viewLifecycleOwner, Observer {
            if (it == true) {
                progress = KProgressHUD.create(requireContext())
                    .setStyle(KProgressHUD.Style.SPIN_INDETERMINATE)
                    .setLabel("Por Favor, espere")
                    .setCancellable(false)
                    .setAnimationSpeed(2)
                    .setDimAmount(0.5f)
                    .show()
            } else {
                progress.dismiss()
            }
        })

        viewModel.error.observe(viewLifecycleOwner, Observer {
            Toast.makeText(requireContext(), it, Toast.LENGTH_LONG).show()

        })

        viewModel.generos.observe(viewLifecycleOwner, Observer {
            binding.spGenero.adapter =
                ArrayAdapter(requireContext(), android.R.layout.simple_spinner_dropdown_item, it)
            generos = it
        })

        viewModel.success.observe(viewLifecycleOwner, Observer {
            Toast.makeText(requireContext(), it, Toast.LENGTH_LONG).show()
            requireActivity().onBackPressed()
        })
    }


    private fun init() {

        viewModel.obtenerGenero()

        binding.spGenero.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
            override fun onItemSelected(p0: AdapterView<*>?, p1: View?, position: Int, p3: Long) {
                genero = generos[position].genero
            }

            override fun onNothingSelected(p0: AdapterView<*>?) {
                TODO("Not yet implemented")
            }

        }

        binding.btnCrearCuenta.setOnClickListener {
            val nombres = binding.textInputLayoutNombres.editText?.text.toString()
            val apellidos = binding.textInputLayoutApellidos.editText?.text.toString()
            val correo = binding.textInputLayoutCorreo.editText?.text.toString()
            val celular = binding.textInputLayoutCelular.editText?.text.toString()
            val documento = binding.textInputLayoutNumeroDocumento.editText?.text.toString()
            val contrasenia = binding.textInputLayoutContrasenia.editText?.text.toString()

            viewModel.registroUsuario(
                RegistroRequest(
                    nombres,
                    apellidos,
                    correo,
                    contrasenia,
                    celular,
                    genero,
                    documento
                )
            )
        }


        binding.includeRegistro.imgAtras.setOnClickListener {
            requireActivity().onBackPressed()
        }
    }
}