package com.santini.appmarket.presentation.producto.cabecera

import android.os.Bundle
import android.util.Log
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.fragment.app.viewModels
import androidx.lifecycle.Observer
import androidx.navigation.Navigation
import androidx.recyclerview.widget.GridLayoutManager
import com.kaopiz.kprogresshud.KProgressHUD
import com.santini.appmarket.R
import com.santini.appmarket.core.SecurityPrefences
import com.santini.appmarket.core.SecurityPrefences.encrypterPreferences
import com.santini.appmarket.databinding.FragmentProductoBinding
import com.santini.appmarket.util.Constantes


class ProductoFragment : Fragment() {

    private var uuiCategoria: String = ""
    private val adapater: ProductoAdapater by lazy {
        ProductoAdapater() {
            val direction = ProductoFragmentDirections.actionProductoFragmentToDetalleProductoFragment(it,uuiCategoria)
            Navigation.findNavController(globalView).navigate(direction)
        }
    }
    private lateinit var binding: FragmentProductoBinding
    private val viewModel: ProductoViewModel by viewModels()
    private var token = ""
    private lateinit var progress: KProgressHUD
    private lateinit var globalView: View


    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_producto, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        globalView = view
        binding = FragmentProductoBinding.bind(view)
        init()
        getArgs()
        setUpAdapter()
        loadData()
        setupObservables()
    }
    private fun init() {
        val preferences = requireContext().encrypterPreferences(Constantes.PREFERENCIA_LOGIN)
        token = SecurityPrefences.getToken(preferences)
    }
    private fun loadData() {
        viewModel.obtenerProducto(token, uuiCategoria)
        Log.d("token", "token en prodcuto Fragment: ${Constantes.BEARER} $token")
    }
    private fun setUpAdapter() {
        binding.rvProductos.adapter = adapater
        binding.rvProductos.layoutManager = GridLayoutManager(requireContext(), 2)
    }
    private fun getArgs() {
        arguments?.let {
            uuiCategoria = ProductoFragmentArgs.fromBundle(it).uuidCategoria
            Toast.makeText(requireContext(), uuiCategoria, Toast.LENGTH_SHORT).show()
        }
    }
    private fun setupObservables() {
        viewModel.loader.observe(viewLifecycleOwner, Observer {
            if (it == true) {
                progress = KProgressHUD.create(requireContext())
                    .setStyle(KProgressHUD.Style.SPIN_INDETERMINATE)
                    .setLabel("Por Favor, espere")
                    .setCancellable(false)
                    .setAnimationSpeed(2)
                    .setDimAmount(0.5f)
                    .show()
            } else {
                progress.dismiss()
            }
        })
        viewModel.error.observe(viewLifecycleOwner, Observer {
            Toast.makeText(requireContext(), it, Toast.LENGTH_LONG).show()
        })
        viewModel.productos.observe(viewLifecycleOwner, Observer {
            //RECIBO LA LISTA Y DEBERIA PASARSELO AL ADAPTER PARA QUE LO PINTE EN MI LISTA
            // adapter.updateLista(it)
            adapater.updateLista(it)
        })
    }
}