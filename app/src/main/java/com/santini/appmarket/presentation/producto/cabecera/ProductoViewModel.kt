package com.santini.appmarket.presentation.producto.cabecera

import android.util.Log
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.santini.appmarket.data.Api
import com.santini.appmarket.data.CarritoDao
import com.santini.appmarket.model.Producto
import com.santini.appmarket.util.Constantes
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import javax.inject.Inject

@HiltViewModel
class ProductoViewModel
    @Inject
    constructor()
    :ViewModel() {
    private val _loader = MutableLiveData<Boolean>()
    val loader: LiveData<Boolean> = _loader

    private val _error = MutableLiveData<String>()
    val error: LiveData<String> = _error

    private val _productos = MutableLiveData<List<Producto>>()
    val productos: LiveData<List<Producto>> = _productos

    fun obtenerProducto(token: String, uuiCategoria: String) {
        viewModelScope.launch {
            _loader.value = true
            try {
                val resultado = withContext(Dispatchers.IO) {
                    Log.d("token", "token: ${Constantes.BEARER} $token y categoria $uuiCategoria")
                    Api.build().obtenerProductos("${Constantes.BEARER} $token", uuiCategoria)
                }
                if (resultado.isSuccessful) {
                    _productos.value = resultado.body()?.data
                } else {
                    _error.value = resultado.message()
                }
            } catch (ex: Exception) {
                _error.value = ex.message
            } finally {
                _loader.value = false
            }
        }
    }
}