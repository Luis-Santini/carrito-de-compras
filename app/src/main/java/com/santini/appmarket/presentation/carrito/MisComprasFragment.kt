package com.santini.appmarket.presentation.carrito

import android.app.AlertDialog
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.os.Bundle
import android.view.View
import android.widget.Button
import android.widget.ImageView
import android.widget.TextView
import android.widget.Toast
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.lifecycle.Observer
import androidx.navigation.Navigation
import androidx.recyclerview.widget.LinearLayoutManager
import com.santini.appmarket.R
import com.santini.appmarket.databinding.FragmentMisComprasBinding
import com.santini.appmarket.model.CarritoDbDto
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class MisComprasFragment : Fragment(R.layout.fragment_mis_compras) {
    private lateinit var binding: FragmentMisComprasBinding
    private lateinit var globalView: View
    private val viewModel: CarritoViewModel by viewModels()
    private lateinit var alertDialog: AlertDialog
    private val adaptador: CarritoAdapter by lazy {
        CarritoAdapter() {

            var total = 0.0
            total += (it.precio * it.cantidad)
            val totalCosto = binding.tvTotal.text.toString().substring(2, binding.tvTotal.text.toString().length).toDouble()

            val nuevoTotal = totalCosto - total
            val nuevoTotalformateado = "%.2f".format(nuevoTotal)
            binding.tvTotal.text = "$.${nuevoTotalformateado}"
            viewModel.eliminarProductoCarrito(it)


        }
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        binding = FragmentMisComprasBinding.bind(view)
        globalView = view

        init()
        setupAdapter()
        loadData()
        setupObserver()
    }

    private fun setupObserver() {
        viewModel.loader.observe(viewLifecycleOwner, Observer {
            if (it == true) {
                binding.progressBar.visibility = View.VISIBLE
            } else {

                binding.progressBar.visibility = View.GONE
            }
        })

        viewModel.error.observe(viewLifecycleOwner, Observer {
            Toast.makeText(requireContext(), it, Toast.LENGTH_LONG).show()
        })

        viewModel.carrito.observe(viewLifecycleOwner, Observer {
            adaptador.updateLista(it)
        })

        viewModel.mensajeProductoEliminado.observe(viewLifecycleOwner, Observer {
            Toast.makeText(requireContext(), it, Toast.LENGTH_LONG).show()
        })

        viewModel.mensajeProductoActualizado.observe(viewLifecycleOwner, Observer {

            Toast.makeText(requireContext(), "hola  $it", Toast.LENGTH_LONG).show()

            alertDialog.dismiss()
        })
    }

    private fun loadData() {
        //viewModel.obtenerProductoCarrito()
        binding.progressBar.visibility = View.VISIBLE
        viewModel.productoCarrito.observe(viewLifecycleOwner, Observer {
            it?.let {


                //totalizar productos

                var total = 0.0
//                for (item in it){
//                    total += (item.precio * item.cantidad)
//                }
//
                it.forEach { item ->    //FOR EACH KOTLIN
                    total += (item.precio * item.cantidad)
                }
                val formateado = "%.2f".format(total)
                binding.tvTotal.text = "$ $formateado"
                adaptador.updateLista(it)
                binding.progressBar.visibility = View.GONE
            }
        })
    }


    private fun setupAdapter() {
        binding.rvCarrito.adapter = adaptador
        binding.rvCarrito.layoutManager = LinearLayoutManager(requireContext())
        adaptador.callBackItemEditar = {
            createDialog(it).show()
        }
    }

    private fun createDialog(producto: CarritoDbDto): AlertDialog {
        val builder = AlertDialog.Builder(requireContext())
        val view = layoutInflater.inflate(R.layout.dialog_editar_producto, null)
        builder.setView(view)
        val imgProductoDialog: ImageView = view.findViewById(R.id.imgProductoDialog)
        val tvProductoDialog: TextView = view.findViewById(R.id.tvProductoDialog)
        val btnActualizar: Button = view.findViewById(R.id.btnActualizar)
        val tvCantidadDialog: TextView = view.findViewById(R.id.tvCantidadDialog)
        val tvPrecioDialog: TextView = view.findViewById(R.id.tvPrecioDialog)
        val btnSumarDialog: ImageView = view.findViewById(R.id.btnSumarDialog)
        val btnRestarDialog: ImageView = view.findViewById(R.id.btnRestarDialog)
        alertDialog = builder.create()
        tvProductoDialog.text = producto.descripcion
        tvCantidadDialog.text = producto.cantidad.toString()
        val total = producto.precio * producto.cantidad
        val totalFormat = "%.2f".format(total)
        tvPrecioDialog.text = totalFormat
        imgProductoDialog.setImageBitmap(convertByesToImage(producto.imagen))
        btnSumarDialog.setOnClickListener {
            val cantidad = tvCantidadDialog.text.toString().toInt() + 1
            tvCantidadDialog.text = "$cantidad"
            var total = 0.0
            total += (producto.precio * cantidad)
          //  val totalDialog = binding.tvTotal.text.toString().toInt()
            val totalFormateado = "%.2f".format( total)
            tvPrecioDialog.text = "$totalFormateado"
            binding.tvTotal.text = "$totalFormateado"
        }
        btnRestarDialog.setOnClickListener {
            val cantidad = tvCantidadDialog.text.toString().toInt() - 1
            tvCantidadDialog.text = "$cantidad"
            var total = 0.0
            total += (producto.precio * cantidad)
           // val totalDialog = binding.tvTotal.text.toString().toInt()
            val totalFormateado = "%.2f".format( total)
            tvPrecioDialog.text = "$totalFormateado"
            binding.tvTotal.text = "$totalFormateado"
        }

        btnActualizar.setOnClickListener {
            viewModel.actualizar(
                CarritoDbDto(
                    producto.uuid,
                    producto.descripcion,
                    producto.precio,
                    tvCantidadDialog.text.toString().toInt(),
                    producto.imagen,
                    producto.codigoCategoria
                )
            )
        }


        return alertDialog


    }


    private fun init() {
        binding.btnCheckIn.setOnClickListener {
            Navigation.findNavController(globalView).navigate(R.id.action_misComprasFragment_to_pagoFragment)
        }
    }


    fun convertByesToImage(byteArray: ByteArray): Bitmap {
        return BitmapFactory.decodeByteArray(
            byteArray, 0, byteArray.size
        )
    }

}