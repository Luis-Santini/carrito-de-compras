package com.santini.appmarket.presentation.pago

import android.app.AlertDialog
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.widget.Toast
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.lifecycle.Observer
import androidx.navigation.Navigation
import com.santini.appmarket.R
import com.santini.appmarket.core.SecurityPrefences
import com.santini.appmarket.core.SecurityPrefences.encrypterPreferences
import com.santini.appmarket.databinding.DialogoMensajeSatisfactorioBinding
import com.santini.appmarket.databinding.FragmentPagoBinding
import com.santini.appmarket.model.*
import com.santini.appmarket.util.Constantes
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class PagoFragment : Fragment(R.layout.fragment_pago) {
    private lateinit var binding : FragmentPagoBinding
    private lateinit var bindingAlerta : DialogoMensajeSatisfactorioBinding
   // private val viewModel: PagoViewModel by viewModels()
    private val viewModel : PagoViewModel by viewModels()
    private lateinit var alertDialog: AlertDialog
    private lateinit var globalView: View
    private var carrito = listOf<CarritoDbDto>()
    private var statusDireccion = 0
    private var statusFecha = 0
    private var statusPago = 0

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        binding = FragmentPagoBinding.bind(view)
        globalView = view
        init()
        setupObservables()
    }

    private fun setupObservables() {

        viewModel.loader.observe(viewLifecycleOwner, Observer {

            with(binding) {
                if (it) {
                    progressPago.visibility = View.VISIBLE
                } else {
                    progressPago.visibility = View.GONE
                }
            }
        })

        viewModel.error.observe(viewLifecycleOwner, Observer {
            Toast.makeText(requireContext(), it.toString(), Toast.LENGTH_SHORT).show()
        })

        /*viewModel.ordenCompra.observe(viewLifecycleOwner, Observer {
            createDialog(it).show()
        })*/
       /* with(binding) {
            viewModel.loader.observe(viewLifecycleOwner, Observer {
                if (it) {
                    progressPago.visibility = View.VISIBLE
                } else {
                    progressPago.visibility = View.GONE
                }
            })
        }*/

        viewModel.ordenCompra.observe(viewLifecycleOwner, Observer {
            createDialog(it).show()
        })


    }

    private fun createDialog(orden: String): AlertDialog {
        bindingAlerta =
            DialogoMensajeSatisfactorioBinding.inflate(LayoutInflater.from(requireContext()))
        val builder = AlertDialog.Builder(requireContext())
        builder.setView(bindingAlerta.root)

        alertDialog = builder.create()

        bindingAlerta.tvOrdenCompra.text = "orden de compra ${orden}"

        bindingAlerta.imgCerrar.setOnClickListener {
            alertDialog.dismiss()
            Navigation.findNavController(globalView)
                .navigate(R.id.action_pagoFragment_to_categoriasFragment)
        }
        bindingAlerta.btnAceptar.setOnClickListener {
            alertDialog.dismiss()
            Navigation.findNavController(globalView)
                .navigate(R.id.action_pagoFragment_to_categoriasFragment)
        }
        return alertDialog
    }

    private fun init() = with(binding) {

        // recupero token
        val preferencia = requireContext().encrypterPreferences(Constantes.PREFERENCIA_LOGIN)
        val token = SecurityPrefences.getToken(preferencia)

        imgSeccionDireccion.setOnClickListener {
            if (statusDireccion == 0) {
                viewSeccionDireccion.visibility = View.VISIBLE
                statusDireccion = 1
            } else {
                viewSeccionDireccion.visibility = View.GONE
                statusDireccion = 0
            }
        }
        imgSeccionFecha.setOnClickListener {
            if (statusFecha == 0) {
                viewSeccionFecha.visibility = View.VISIBLE
                statusFecha = 1
            } else {
                viewSeccionFecha.visibility = View.GONE
                statusFecha = 0
            }
        }
        imgTipoPago.setOnClickListener {
            if (statusPago == 0) {
                viewSeccionTipoPago.visibility = View.VISIBLE
                statusPago = 1
            } else {
                viewSeccionTipoPago.visibility = View.GONE
                statusPago = 0
            }
        }


        btnPagar.setOnClickListener {
            //Datos de ingreso
            val tipoEntrega = if(rbCasa.isChecked) 1 else if(rbOficina.isChecked) 2 else 3
            val direccion = edtDireccion.text.toString()
            val referencia = edtReferencia.text.toString()
            val distrito = edtDistrito.text.toString()

            val fecha = edtFecha.text.toString()
            val hora = edtHora.text.toString()

            val tipoPago = if (rbContraEntrega.isChecked) 1 else 2
            val montoPagar = edtMontoPagar.text.toString()

            val montoTotal = tvTotalPagar.text.toString()

            val direccionEnvio = DireccionEnvio(tipoEntrega,direccion,referencia,distrito)
            val metodoEnvio = MetodoEnvio(tipoPago,montoPagar.toDouble())
            val fechaHora = "$fecha $hora"

            val productoEnvio = carrito.map {
                ProductoEnvio(it.codigoCategoria,it.uuid,it.cantidad)
            }

            val pagoRequest = PagoRequest(direccionEnvio,metodoEnvio,fechaHora,productoEnvio,montoTotal.toDouble())

            viewModel.pagarCarrito(token,pagoRequest)
        }
       obtenerProductosCarrito()
    }

    private fun obtenerProductosCarrito() = with(binding) {
        progressPago.visibility = View.VISIBLE

        viewModel.productosCarritos.observe(viewLifecycleOwner, Observer {
            carrito = it
            var total = 0.0
            it.forEach {
                total += (it.precio * it.cantidad)
            }
            val totalFormateado = "%.2f".format(total)
            tvTotalPagar.text = "$totalFormateado"
            progressPago.visibility = View.GONE
        })
    }
}




