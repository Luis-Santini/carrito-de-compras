package com.santini.appmarket.presentation.registro

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.santini.appmarket.data.Api
import com.santini.appmarket.model.Genero
import com.santini.appmarket.model.RegistroRequest
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext

import kotlin.Exception

class RegistroViewModel : ViewModel() {
    private val _loader = MutableLiveData<Boolean>()
    val loader: LiveData<Boolean> = _loader


    private val _error = MutableLiveData<String>()
    val error: LiveData<String> = _error

    private val _generos = MutableLiveData<List<Genero>>()
    val generos: LiveData<List<Genero>> = _generos

    private val _success = MutableLiveData<String>()
    val success: LiveData<String> = _success

    fun obtenerGenero() {

        viewModelScope.launch(Dispatchers.Main) {
            _loader.value = true
            try {
                val resultado = withContext(Dispatchers.IO) {
                    Api.build().obtenerGeneros()
                }
                if (resultado.isSuccessful) {
                    _generos.value = resultado.body()?.data
                } else {
                    _error.value = resultado.message().toString()
                }

            } catch (ex: Exception) {
                _error.value = ex.message.toString()
            } finally {
                _loader.value = false
            }
        }


    }

    fun registroUsuario(registroRequest: RegistroRequest) {
        viewModelScope.launch {
            _loader.value = true

            try {
              val resultado = withContext(Dispatchers.IO){
                 Api.build().grabarUsuario(registroRequest)
               }
                if (resultado.isSuccessful){
                    if(resultado.body()?.success == true){
                        _success.value = resultado.body()?.message
                    }else{
                        _error.value = resultado.body()?.message
                    }

                }else{
                    _error.value = resultado.message().toString()
                }
            }catch (ex: Exception){
                _error.value = ex.message.toString()
            }finally {
                _loader.value = false
            }

        }
    }


}