package com.santini.appmarket.presentation.producto.detalle

import android.graphics.Bitmap
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.View
import android.widget.ImageView
import android.widget.Toast
import androidx.core.graphics.drawable.toBitmap
import androidx.fragment.app.viewModels
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.LinearLayoutManager
import com.santini.appmarket.R
import com.santini.appmarket.databinding.FragmentDetalleProductoBinding
import com.santini.appmarket.model.CarritoDbDto
import com.santini.appmarket.model.Producto
import com.squareup.picasso.Picasso
import dagger.hilt.android.AndroidEntryPoint
import dagger.hilt.android.EarlyEntryPoint
import java.io.ByteArrayOutputStream

@AndroidEntryPoint
class DetalleProductoFragment : Fragment(R.layout.fragment_detalle_producto) {

    private lateinit var binding: FragmentDetalleProductoBinding
    private val viewModel: DetalleProductoViewModel by viewModels()
    private var codigoCategoria: String = ""
    private lateinit var producto: Producto
    private val adaptador by lazy {
        DetalleProductoAdapter()
    }


    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        binding = FragmentDetalleProductoBinding.bind(view)
        init()
        getArgs()
        setUpAdapter()
        setupOserver()
    }

    private fun setupOserver() {
        viewModel.error.observe(viewLifecycleOwner, Observer {
            Toast.makeText(requireContext(), it.toString(), Toast.LENGTH_SHORT).show()
        })
        with(binding) {
            viewModel.loader.observe(viewLifecycleOwner, Observer {
                if (it) {
                    progressBarCarrito.visibility = View.VISIBLE
                } else {
                    progressBarCarrito.visibility = View.GONE
                }
            })
        }

        viewModel.respuesta.observe(viewLifecycleOwner, Observer {
            Toast.makeText(requireContext(), it.toString(), Toast.LENGTH_SHORT).show()
        })
    }

    fun convertImageToBytes(imageView: ImageView): ByteArray {
        val bitmap = imageView.drawable.toBitmap()
        val stream = ByteArrayOutputStream()
        bitmap.compress(Bitmap.CompressFormat.PNG, 100, stream)
        return stream.toByteArray()
    }

    private fun init() {
        binding.btnAgregar.setOnClickListener {
            // Grabar en bd
            val cantidad = binding.tvCantidad.text.toString().toInt()

            val dto = CarritoDbDto(
               // 0,
                producto.uuid,
                producto.descripcion,
                producto.precio,
                cantidad,
                convertImageToBytes(binding.imgProductoPrincipal),
                codigoCategoria
            )
            viewModel.grabarProdutoCarrito(dto)

        }


        binding.btnSumar.setOnClickListener {
            val cantidad = binding.tvCantidad.text.toString().toInt() + 1
            binding.tvCantidad.text = "$cantidad"

        }

        binding.btnRestar.setOnClickListener {
            val cantidad = binding.tvCantidad.text.toString().toInt() - 1
            binding.tvCantidad.text = "$cantidad"
        }
    }

    private fun setUpAdapter() {
        binding.rvProductosSecundarios.adapter = adaptador
        binding.rvProductosSecundarios.layoutManager = LinearLayoutManager(requireContext())
    }

    private fun getArgs() {
        arguments?.let {
            producto = DetalleProductoFragmentArgs.fromBundle(it).producto
            codigoCategoria = DetalleProductoFragmentArgs.fromBundle(it).codigoCategoria
            producto?.let {
                with(binding) {
                    tvDescripcionDetalle.text = it.descripcion
                    tvPrecioDetalle.text = "$ ${it.precio}"
                    tvCaracteristicasDetalle.text = it.caracteristicas
                    Picasso.get().load(it.imagenes[0]).error(R.drawable.logo_bienvenida)
                        .into(imgProductoPrincipal)
                    // pinte las imagenes de recycler
                    adaptador.updateLista(producto.imagenes)

                }

            }

        }
    }

}