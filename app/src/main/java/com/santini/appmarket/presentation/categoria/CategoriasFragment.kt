package com.santini.appmarket.presentation.categoria

import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.lifecycle.Observer
import androidx.navigation.Navigation
import androidx.recyclerview.widget.LinearLayoutManager
import com.kaopiz.kprogresshud.KProgressHUD
import com.santini.appmarket.R
import com.santini.appmarket.core.SecurityPrefences
import com.santini.appmarket.core.SecurityPrefences.encrypterPreferences
import com.santini.appmarket.databinding.FragmentCategoriasBinding
import com.santini.appmarket.util.Constantes


class CategoriasFragment : Fragment() {
    private lateinit var binding: FragmentCategoriasBinding
    private val viewModel: CategoriaViewModel by viewModels()
    private var token = ""
    private lateinit var progress: KProgressHUD
    private lateinit var globalView: View
    //referenciar adapter
    private val adapter: CategoriaAdapter by lazy {
        CategoriaAdapter() {
            //it.uuid
           val directions =
                CategoriasFragmentDirections.actionCategoriasFragmentToProductoFragment(it.uuid)
            Navigation.findNavController(globalView).navigate(directions)
        }
    }


    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment

        return inflater.inflate(R.layout.fragment_categorias, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        binding = FragmentCategoriasBinding.bind(view)
        globalView = view
        init()
        setupAdapter()
        loadData()
        setupObservables()
    }

    private fun setupObservables() {
        viewModel.loader.observe(viewLifecycleOwner, Observer {
            if (it == true) {
                progress = KProgressHUD.create(requireContext())
                    .setStyle(KProgressHUD.Style.SPIN_INDETERMINATE)
                    .setLabel("Por Favor, espere")
                    .setCancellable(false)
                    .setAnimationSpeed(2)
                    .setDimAmount(0.5f)
                    .show()
            } else {
                progress.dismiss()
            }
        })

        viewModel.error.observe(viewLifecycleOwner, Observer {
            Toast.makeText(requireContext(), it, Toast.LENGTH_LONG).show()
        })

        viewModel.categorias.observe(viewLifecycleOwner, Observer {
            //RECIBO LA LISTA Y DEBERIA PASARSELO AL ADAPTER PARA QUE LO PINTE EN MI LISTA
            adapter.updateLista(it)
        })
    }

    private fun init() {

        val preferences = requireContext().encrypterPreferences(Constantes.PREFERENCIA_LOGIN)
        token = SecurityPrefences.getToken(preferences)
        Log.d("token", "token en categoria Fragment: ${Constantes.BEARER} $token")
    }

    private fun loadData() {

        viewModel.obtenerCategorias(token)

    }

    private fun setupAdapter() {

        binding.rvCategirias.adapter = adapter
        binding.rvCategirias.layoutManager = LinearLayoutManager(requireContext())


    }

}