package com.santini.appmarket.data

import com.santini.appmarket.model.*
import com.santini.appmarket.util.Constantes
import retrofit2.Response
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import retrofit2.http.*

object Api {

    // 1 configurar la url base
    // URL BASE
    // https://marketapp2021.herokuapp.com/
    // METODO
    // api/usuarios/login
    private val builder: Retrofit.Builder = Retrofit.Builder()
        .baseUrl(Constantes.BASE_URL)
        .addConverterFactory(GsonConverterFactory.create())


    //2 configurar los metodos (se crea interfaces y se dice que se quiere hacer, no importa el como)
    interface ApiInterface {
        @POST("api/usuarios/login")
        suspend fun autenticar(@Body loginRequest: LoginRequest): Response<LoginNetworkDto>

        @GET("api/usuarios/obtener-generos")
        suspend fun obtenerGeneros(): Response <GeneroNetworkDto>

        @POST("api/usuarios/crear-cuenta")
        suspend fun grabarUsuario(@Body registroRequest: RegistroRequest):Response<RegistroNetworkDto>

        @GET("api/categorias")
        suspend fun obtenerCategorias(@Header ("Authorization")authorization: String): Response <CategoriasNetworkDto>


        @GET("api/categorias/{categoriaId}/productos")// consulta get con path
        suspend fun obtenerProductos(
            @Header("Authorization") authorization: String,
            @Path("categoriaId") categoriaId: String
        )
                : Response<ProductoNetworkDto>

        @POST("api/compras/nueva-compra")
        suspend fun pagar(@Header("Authorization") authorization: String,
                            @Body pagoRequest: PagoRequest) : Response <PagoNetworkDto>

    }

    // 3 Regresar una instancia de los metodos
    fun build(): ApiInterface {
        val apiInterface = builder.build().create(ApiInterface::class.java)
        return apiInterface
    }




}