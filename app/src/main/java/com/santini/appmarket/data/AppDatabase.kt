package com.santini.appmarket.data

import androidx.room.Database
import androidx.room.RoomDatabase
import com.santini.appmarket.model.CarritoDbDto

@Database(entities = [CarritoDbDto::class], version = 1, exportSchema = false)
abstract class AppDatabase : RoomDatabase(){

    //Registrar los Daos (si uso inyeccion de dependecia) sino hay que creer la base de datos

    abstract fun carritoDao():CarritoDao


}