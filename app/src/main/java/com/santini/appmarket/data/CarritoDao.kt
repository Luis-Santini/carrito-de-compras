package com.santini.appmarket.data

import androidx.lifecycle.LiveData
import androidx.room.*
import com.santini.appmarket.model.CarritoDbDto

@Dao
interface CarritoDao {
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertar(carritoDbDto: CarritoDbDto): Long

    @Update
    fun actualizar(carritoDbDto: CarritoDbDto)

    @Query("select *from carrito order by uuid")
    fun obtenerProductosCarrito() : LiveData<List<CarritoDbDto>>


    @Delete   //elimina un solo producto
    fun eliminar(carritoDbDto: CarritoDbDto)

    @Delete  // elimina todos los productos (se usa cuando se paga la compra
    fun eliminarProductosCarrito (carritoDbDto: List<CarritoDbDto>)
}