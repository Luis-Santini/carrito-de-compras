package com.santini.appmarket.model

import com.google.gson.annotations.SerializedName

data class LoginRequest(
    @SerializedName("email")
    val email: String,
    @SerializedName("password")
    val password: String
)

data class RegistroRequest(
    @SerializedName("nombres")
    val nombres: String,
    @SerializedName("apellidos")
    val apellidos: String,
    @SerializedName("email")
    val email: String,
    @SerializedName("password")
    val password: String,
    @SerializedName("celular")
    val celular: String,
    @SerializedName("genero")
    val genero: String,
    @SerializedName("nroDoc")
    val nroDoc: String

)


 // PARA ENVIAR LOS PRODUCTOS COMPRADOS /////
 data class PagoRequest(
     val direccionEnvio: DireccionEnvio,
     val metodoPago : MetodoEnvio,
     val fechaHora:String,
     val productos: List<ProductoEnvio>,
     val total:Double
 )

data class DireccionEnvio(
    val tipo:Int,
    val direccion:String,
    val referencia:String,
    val distrito:String
)

data class MetodoEnvio(
    val tipo:Int,
    val monto:Double
)

data class ProductoEnvio(
    val categoriaId:String,
    val productoId:String,
    val cantidad:Int
)

data class EliminarCategoriaRequest(
    val categoriaId:String
)



