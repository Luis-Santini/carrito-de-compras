package com.santini.appmarket.model

import com.google.gson.annotations.SerializedName
import java.io.Serializable


data class Usuario(
    @SerializedName("uuid")
    val uuid: String,
    @SerializedName("nombres")
    val nombres: String,
    @SerializedName("apellidos")
    val apellidos: String,
    @SerializedName("email")
    val email: String,
    @SerializedName("celular")
    val celular: String,
    @SerializedName("genero")
    val genero: String,
    @SerializedName("nroDoc")
    val nroDoc: String
)

data class Genero(
    @SerializedName("descripcion")
    val descripcion: String,
    @SerializedName("genero")
    val genero: String
){
    override fun toString(): String {
        return "$descripcion"
    }
}

data class Categoria(
    @SerializedName("cover")
    val cover: String,
    @SerializedName("nombre")
    val nombre: String,
    @SerializedName("uuid")
    val uuid: String
)

data class Producto(
    @SerializedName("cantidad")
    val cantidad: Int,
    @SerializedName("caracteristicas")
    val caracteristicas: String,
    @SerializedName("codigo")
    val codigo: String,
    @SerializedName("descripcion")
    val descripcion: String,
    @SerializedName("imagenes")
    val imagenes: List<String>,
    @SerializedName("precio")
    val precio: Double,
    @SerializedName("stock")
    val stock: Int,
    @SerializedName("uuid")
    val uuid: String
) :Serializable