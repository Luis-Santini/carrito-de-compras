package com.santini.appmarket.model

import androidx.annotation.NonNull
import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity(tableName = "carrito")
data class CarritoDbDto(

//    @NonNull
//    @ColumnInfo(name = "codigo")
//    val codigo: Int,
//

    @PrimaryKey (autoGenerate = false)
    @NonNull
    @ColumnInfo(name = "uuid")
    val uuid: String,
    @ColumnInfo(name = "descripcion")
    val descripcion: String,
    @ColumnInfo(name = "precio")
    val precio: Double,
    @ColumnInfo(name = "cantidad")
    val cantidad: Int,
    @ColumnInfo(name = "imagen")
    val imagen: ByteArray,
    @ColumnInfo(name = "codigoCategoria")
    val codigoCategoria: String
){ // voy a crear un constructor para poder eliminar todos los elementos del carrito pero solo pasando el uuid
    constructor(uuid: String) : this(uuid,"", 0.0, 0, byteArrayOf(), "")
}