package com.santini.appmarket.model

import com.google.gson.annotations.SerializedName

data class LoginNetworkDto(
    @SerializedName("success")
    val success: Boolean,
    @SerializedName("message")
    val message: String,
    @SerializedName("token")
    val token: String,
    @SerializedName("data")
    val data: Usuario
)

data class GeneroNetworkDto(
    @SerializedName("data")
    val data: List<Genero>,
    @SerializedName("message")
    val message: String,
    @SerializedName("success")
    val success: Boolean
)

data class RegistroNetworkDto(
    @SerializedName("success")
    val success: Boolean,
    @SerializedName("message")
    val message: String,


)

data class CategoriasNetworkDto(
    @SerializedName("data")
    val data: List<Categoria>,
    @SerializedName("message")
    val message: String,
    @SerializedName("success")
    val success: Boolean
)

data class ProductoNetworkDto(
    @SerializedName("data")
    val data: List<Producto>,
    @SerializedName("message")
    val message: String,
    @SerializedName("success")
    val success: Boolean
)

data class PagoNetworkDto(
    val success: Boolean,
    val message: String,
    val data: String
)




