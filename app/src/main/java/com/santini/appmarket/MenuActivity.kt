package com.santini.appmarket

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.core.view.GravityCompat
import androidx.navigation.NavAction
import androidx.navigation.NavController
import androidx.navigation.NavDestination
import androidx.navigation.Navigation
import androidx.navigation.ui.NavigationUI
import com.santini.appmarket.databinding.ActivityMenuBinding
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class MenuActivity : AppCompatActivity() {
    private lateinit var binding: ActivityMenuBinding
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityMenuBinding.inflate(layoutInflater)
        setContentView(binding.root)
        //configurar el controlador
        val navController = Navigation.findNavController(this, R.id.menu_nav_host_frament)
        NavigationUI.setupWithNavController(binding.navigationView, navController)

        binding.imgMenu.setBackgroundResource(R.drawable.icono_menu)

        binding.imgMenu.setOnClickListener {

            when (navController.currentDestination?.id) {
                R.id.productoFragment -> {
                    onBackPressedDispatcher.onBackPressed()
                    binding.imgMenu.setBackgroundResource(R.drawable.icono_atras)
                }
                R.id.detalleProductoFragment -> {
                    onBackPressedDispatcher.onBackPressed()

                }
                else ->binding.drawerLayout.openDrawer(GravityCompat.START)
            }

        }

        //detecta cuando se hace click en algun elemento del menu lateral
        navController.addOnDestinationChangedListener(object :
            NavController.OnDestinationChangedListener {
            override fun onDestinationChanged(
                controller: NavController,
                destination: NavDestination,
                arguments: Bundle?
            ) {
                binding.tvTituloMenu.text = destination.label
                when (destination.label) {
                    "Producto" -> {
                        binding.imgMenu.setBackgroundResource(R.drawable.icono_atras)
                    }
                    "Detalle Producto" -> {
                        binding.imgMenu.setBackgroundResource(R.drawable.icono_atras)
                    }
                }
            }

        })
    }
}