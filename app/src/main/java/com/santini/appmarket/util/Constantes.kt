package com.santini.appmarket.util

class Constantes {
    companion object{
        val PREFERENCIA_LOGIN = "preferencia_login"
        val TOKEN = "token"
        val BASE_URL = "https://marketapp2021.herokuapp.com/"
        val BEARER = "Bearer"
        }
}