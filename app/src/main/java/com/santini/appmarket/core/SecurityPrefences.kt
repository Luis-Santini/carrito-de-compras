package com.santini.appmarket.core

import android.content.Context
import android.content.SharedPreferences
import androidx.security.crypto.EncryptedSharedPreferences
import androidx.security.crypto.MasterKeys
import com.santini.appmarket.util.Constantes

object SecurityPrefences {

    fun Context.encrypterPreferences(preferencesName: String) : SharedPreferences{
        val masterKeys : String  = MasterKeys.getOrCreate(MasterKeys.AES256_GCM_SPEC)
       return EncryptedSharedPreferences.create(
            preferencesName,
            masterKeys,
            this,
            EncryptedSharedPreferences.PrefKeyEncryptionScheme.AES256_SIV,
            EncryptedSharedPreferences.PrefValueEncryptionScheme.AES256_GCM
        )
    }

    fun saveToken (token:String, encyptedPreferences:SharedPreferences){
        encyptedPreferences.edit().putString(Constantes.TOKEN, token).apply()
    }

    fun getToken(encyptedPreferences: SharedPreferences) : String{
        return encyptedPreferences.getString(Constantes.TOKEN,"") ?:""
    }

}